//
//  CollectionViewCellProtocol.swift
//  WEATHER
//
//  Created by Даниил Чемеркин on 15.01.2020.
//  Copyright © 2020 polydev.io. All rights reserved.
//

import Foundation
import UIKit

protocol CollectionViewCellProtocol {
    static var coefficient: CGFloat { get }
    static var id: String { get }
    
    static func register(_ collectionView: UICollectionView)
    static func getCell<CellType: UITableViewCell>(_ collectionView: UICollectionView, for indexPath: IndexPath) -> CellType?
    
    static func getSize(width: CGFloat, indent: CGFloat) -> CGSize
    
}

extension CollectionViewCellProtocol {
    
    static func register(_ collectionView: UICollectionView) {
        collectionView.register(UINib(nibName: self.id, bundle: nil), forCellWithReuseIdentifier: self.id)
    }
    
    static func getCell<CellType>(_ collectionView: UICollectionView, for indexPath: IndexPath) -> CellType? {
        guard
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.id, for: indexPath) as? CellType else {
            return nil
        }
        return cell
    }
    
    static func getSize(width: CGFloat, indent: CGFloat) -> CGSize {
        let cellWidth = width - indent - indent
        
        return CGSize(width: cellWidth, height: cellWidth * self.coefficient)
    }
    
    static var id: String {
        return String(describing: self)
    }
    
}

