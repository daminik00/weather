//
//  TableViewCellProtocol.swift
//  WEATHER
//
//  Created by Даниил Чемеркин on 15.01.2020.
//  Copyright © 2020 polydev.io. All rights reserved.
//

import Foundation
import UIKit

protocol TableViewCellProtocol {
    static var cellHeight: CGFloat { get set }
    static var id: String { get }
    
    static func register(tableView: UITableView)
    static func getCell<CellType: UITableViewCell>(tableView: UITableView) -> CellType?
}

extension TableViewCellProtocol {
    static func register(tableView: UITableView) {
        tableView.register(UINib(nibName: self.id, bundle: nil), forCellReuseIdentifier: self.id)
    }
    
    static func getCell<CellType>(tableView: UITableView) -> CellType? {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: self.id) as? CellType else {
            return nil
        }
        return cell
    }
    
    static var id: String {
        return String(describing: self)
    }
    
}
