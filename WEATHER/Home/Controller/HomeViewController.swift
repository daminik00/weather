//
//  HomeViewController.swift
//  WEATHER
//
//  Created by Даниил Чемеркин on 15.01.2020.
//  Copyright © 2020 polydev.io. All rights reserved.
//

import UIKit

enum SubscriptionState {
    case user
    case nouser
}

class HomeViewController: UIViewController {
    
    var model: [HomeCellModel] = [
        HomeCellModel(title: "Чувствуется как".uppercased(), content: "-1 °С"),
        HomeCellModel(title: "Давление".uppercased(), content: "764.22 мм"),
        HomeCellModel(title: "Вероятность дождя".uppercased(), content: "50%"),
        HomeCellModel(title: "Влажность".uppercased(), content: "96%"),
        HomeCellModel(title: "Ветер".uppercased(), content: "ЮЮЗ 4 м/с"),
    ]
    
    
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.separatorColor  = .clear
        HomeMainCell.register(tableView: tableView)
        HomeCell.register(tableView: tableView)
    }

}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + model.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell: HomeMainCell = HomeMainCell.getCell(tableView: tableView)!
            return cell
        case 1 + model.count:
            let cell = UITableViewCell()
            cell.separatorInset.left = 1000
            return UITableViewCell()
        default:
            let cell: HomeCell = HomeCell.getCell(tableView: tableView)!
            cell.setContent(self.model[indexPath.row - 1])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return HomeMainCell.cellHeight
        case 1 + model.count:
            return 88
        default:
            return HomeCell.cellHeight
        }
    }
    
}

extension HomeViewController: UITableViewDelegate {
    
}
