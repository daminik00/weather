//
//  HomeCellModel.swift
//  WEATHER
//
//  Created by Даниил Чемеркин on 15.01.2020.
//  Copyright © 2020 polydev.io. All rights reserved.
//

import Foundation

struct HomeCellModel {
    var title: String
    var content: String
}
