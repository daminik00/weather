//
//  HomeCell.swift
//  WEATHER
//
//  Created by Даниил Чемеркин on 15.01.2020.
//  Copyright © 2020 polydev.io. All rights reserved.
//

import UIKit

class HomeCell: UITableViewCell, TableViewCellProtocol {
    
    static var cellHeight: CGFloat = 63
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var content: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setContent(_ model: HomeCellModel) {
        self.title.text = model.title
        self.content.text = model.content
    }
}
