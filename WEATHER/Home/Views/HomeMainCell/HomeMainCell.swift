//
//  HomeMainCell.swift
//  WEATHER
//
//  Created by Даниил Чемеркин on 15.01.2020.
//  Copyright © 2020 polydev.io. All rights reserved.
//

import UIKit

class HomeMainCell: UITableViewCell, TableViewCellProtocol {
    
    static var cellHeight: CGFloat = 323
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
