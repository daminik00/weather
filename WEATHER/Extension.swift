//
//  Extension.swift
//  WEATHER
//
//  Created by Даниил Чемеркин on 15.01.2020.
//  Copyright © 2020 polydev.io. All rights reserved.
//

import UIKit

extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            self.layer.masksToBounds = true
        }
    }
    
    @IBInspectable
    var isCircle: Bool {
        set {
            if newValue {
                self.layer.cornerRadius = self.bounds.height / 2
                self.layer.masksToBounds = true
            } else {
                self.layer.cornerRadius = self.cornerRadius
                self.layer.masksToBounds = true
            }
        }
        get {
            return self.layer.cornerRadius == (self.bounds.height / 2)
        }
    }
    
    
}
